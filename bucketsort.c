// qsort.c - Parallel sorting algorithm based on quicksort
// compile: mpicc -Wall -O -o qsort qsort.c
// run:     mpirun -np num_procs qsort in_file out_file

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#define BLOCK_LOW(id,p,n) ((id)*(n)/(p))
#define BLOCK_HIGH(id,p,n) (BLOCK_LOW((id)+1,p,n)-1)
#define BLOCK_SIZE(id,p,n) (BLOCK_HIGH(id,p,n)-BLOCK_LOW(id,p,n)+ 1) 
#define BLOCK_OWNER(index,p,n) (((p)*((index)+1)-1)/(n))


// swap entries in array v at positions i and j; used by quicksort
void swap(int * v, int i, int j)
{
  int t = v[i];
  v[i] = v[j];
  v[j] = t;
}


// (quick) sort slice of array v; slice starts at s and is of length n
void quicksort(int * v, int s, int n)
{
  int x, p, i;
  // base case?
  if (n <= 1)
    return;
  // pick pivot and swap with first element
  x = v[s + n/2];
  swap(v, s, s + n/2);
  // partition slice starting at s+1
  p = s;
  for (i = s+1; i < s+n; i++)
    if (v[i] < x) {
      p++;
      swap(v, i, p);
    }
  // swap pivot into place
    swap(v, s, p);
  // recurse into partition
    quicksort(v, s, p-s);
    quicksort(v, p+1, s+n-p-1);
  }


// merge two sorted arrays v1, v2 of lengths n1, n2, respectively
  int * merge(int * v1, int n1, int * v2, int n2)
  {
    int * result = (int *)malloc((n1 + n2) * sizeof(int));
    int i = 0;
    int j = 0;
    int k;
    for (k = 0; k < n1 + n2; k++) {
      if (i >= n1) {
        result[k] = v2[j];
        j++;
      }
      else if (j >= n2) {
        result[k] = v1[i];
        i++;
      }
    else if (v1[i] < v2[j]) { // indices in bounds as i < n1 && j < n2
      result[k] = v1[i];
      i++;
    }
    else { // v2[j] <= v1[i]
      result[k] = v2[j];
      j++;
    }
  }
  return result;
}


int main(int argc, char ** argv)
{
  int n = 0;
  int * data = NULL;
  int *result;
  int c, s;
  int * chunk;
  int o;
  int * other;
  int step;
  int p, id;
  MPI_Status status;
  double elapsed_time;
  FILE * file = NULL;
  int i;
  int j;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &p);
  MPI_Comm_rank(MPI_COMM_WORLD, &id);

  if (id == 0) {
    // read size of data
    file = fopen(argv[1], "r");
    fscanf(file, "%d", &n);
    // compute chunk size
    c = n/p; if (n%p) c++;
    // read data from file
    data = (int *)malloc(n * sizeof(int));
    for (i = 0; i < n; i++){
      fscanf(file, "%d", &(data[i]));
    }
    fclose(file);
  }

  // start the timer
  MPI_Barrier(MPI_COMM_WORLD);
  elapsed_time = - MPI_Wtime();

  // broadcast size
  MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);

  // broadcast data
  if(id != 0){
    data = (int *)malloc(n * sizeof(int));
  }
  MPI_Bcast(data, n, MPI_INT, 0, MPI_COMM_WORLD);

  chunk = (int *)malloc(n * sizeof(int));
  int chunkCount = 0;

  // Bucket sort
  int lowerbound = BLOCK_LOW(id, p, n);
  int upperbound = BLOCK_HIGH(id, p, n);
  for(i=0; i<n; i++){
    if(lowerbound <= data[i] && data[i] <= upperbound){
      chunk[chunkCount++] = data[i];
    }
  }

  quicksort(chunk, 0, chunkCount);

  if(id == 0){
    int resultCount = 0;
    result = (int *)malloc(n * sizeof(int));
    for(i=0; i<chunkCount; i++){
      result[i] = chunk[i];
      resultCount++;
    }
    for(i=1; i<p; i++){
      int recvCount = 0;
      int * buffer;
      MPI_Recv(&recvCount, 1, MPI_INT, i, 0, MPI_COMM_WORLD,MPI_STATUS_IGNORE);
      buffer = (int *)malloc(recvCount * sizeof(int));
      MPI_Recv(buffer, recvCount, MPI_INT, i, 0, MPI_COMM_WORLD,MPI_STATUS_IGNORE);
      for(j=0; j<recvCount; j++){
        result[resultCount++] = buffer[j];
      }
    }
  }else{
    MPI_Send(&chunkCount, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
    MPI_Send(chunk, chunkCount, MPI_INT, 0, 0, MPI_COMM_WORLD);
  }

  // stop the timer
  elapsed_time += MPI_Wtime();

  // write sorted data to out file and print out timer
  if (id == 0) {
    file = fopen(argv[2], "w");
    fprintf(file, "%d\n", n);   // assert (s == n)
    for (i = 0; i < n; i++)
      fprintf(file, "%d\n", result[i]);
    fclose(file);
    // printf("Quicksort %d ints on %d procs: %f secs\n", n, p, elapsed_time);
    printf("%d %2d %f\n", n, p, elapsed_time);
  }

  MPI_Finalize();
  return 0;
}